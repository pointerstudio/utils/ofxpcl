#pragma once

#include "ofMain.h"

#ifdef nil
 #undef nil
#endif

#include "Types.h"
#include "Utility.h"
#include "Tree.h"

// file io
#include <pcl/io/pcd_io.h>
#include <pcl/io/ply_io.h>
#include <pcl/io/obj_io.h>

// transform
#include <pcl/common/transforms.h>

// thresold
#include <pcl/filters/passthrough.h>

// outlier removal
#include <pcl/filters/statistical_outlier_removal.h>
#include <pcl/filters/radius_outlier_removal.h>

// segmentation
#include <pcl/sample_consensus/model_types.h>

// downsample
#include <pcl/filters/voxel_grid.h>

// segmentation
#include <pcl/ModelCoefficients.h>
#include <pcl/segmentation/sac_segmentation.h>
#include <pcl/sample_consensus/method_types.h>
#include <pcl/filters/extract_indices.h>

// triangulate
#include <pcl/features/normal_3d.h>
#include <pcl/surface/gp3.h>
#include <pcl/surface/grid_projection.h>
#include <pcl/Vertices.h>
#include <pcl/surface/poisson.h>

// mls
#include <pcl/surface/mls.h>
#include <pcl/io/pcd_io.h>

#include <pcl/surface/organized_fast_mesh.h>
#include <pcl/features/integral_image_normal.h>

// registration
#include <pcl/registration/icp.h>
#include <pcl/registration/icp_nl.h>
#include <pcl/registration/transforms.h>

//convenient typedefs
typedef pcl::PointXYZ PointT;
typedef pcl::PointCloud <PointT> PointCloud;
typedef pcl::PointNormal PointNormalT;
typedef pcl::PointCloud <PointNormalT> PointCloudWithNormals;


namespace ofxPCL {


template <typename T>
struct meshAndCloud {
    ofMesh mesh;
    T cloud;
};

template <typename T>
struct meshCloudMatrix {
    ofMesh mesh;
    T cloud;
    Eigen::Matrix4f mat;
};

template <typename T>
struct meshTwoClouds {
    ofMesh mesh;
    T cloud1;
    T cloud2;
};

template <typename T>
struct meshTwoCloudsMatrix {
    ofMesh mesh;
    T cloud1;
    T cloud2;
    Eigen::Matrix4f mat;
};

template <typename T>
meshCloudMatrix <T> combine(T cloud, ofMatrix4x4 matrix){
    assert(cloud);

    if(cloud->points.empty()){
        return;
    }
    Eigen::Matrix4f eigenMat;

    meshCloudMatrix <T> output;
    convert(cloud, output.mesh);

    *output.cloud = *cloud;
    output.mat = eigenMat;
    return output;
}
//
// file io
//
template <typename T>
inline T loadPointCloud(string path){
	T cloud(new typename T::element_type);
	path = ofToDataPath(path);

    if(pcl::io::loadPCDFile <typename T::element_type::PointType>(path.c_str(), *cloud) == -1){
		ofLogError("ofxPCL:loadPointCloud") << "file not found: " << path;
    }

	return cloud;
}

template <typename T>
inline void savePointCloud(string path, T cloud){
	assert(cloud);

    if(cloud->points.empty()){
        return;
    }

	path = ofToDataPath(path);
	pcl::io::savePCDFileBinary(path.c_str(), *cloud);
}



//
// transform
//
template <typename T>
void transform(T cloud, ofMatrix4x4 matrix){
    assert(cloud);

    if(cloud->points.empty()){
        return;
    }
    Eigen::Matrix4f eigenMat;

    //We can shift the column row arrangment like this
    eigenMat << matrix._mat[0][0], matrix._mat[0][1], matrix._mat[0][2], matrix._mat[3][0],
        matrix._mat[1][0], matrix._mat[1][1], matrix._mat[1][2], matrix._mat[3][1],
        matrix._mat[2][0], matrix._mat[2][1], matrix._mat[2][2], matrix._mat[3][2],
        matrix._mat[0][3], matrix._mat[1][3], matrix._mat[2][3], matrix._mat[3][3];
	pcl::transformPointCloud(*cloud, *cloud, eigenMat);

}

template <typename T>
void transformFromEigenMat(T cloud, Eigen::Matrix4d eigenMat){
    assert(cloud);

    if(cloud->points.empty()){
        return;
    }
    pcl::transformPointCloud(*cloud, *cloud, eigenMat);
}

template <typename T>
void transformFromQuat(T cloud, ofVec3f translation, ofQuaternion rotation){
    assert(cloud);

    if(cloud->points.empty()){
        return;
    }
    Eigen::Vector3f offsetEigen;
    Eigen::Quaternionf rotationEigen(rotation._v[0], rotation._v[1], rotation._v[2], rotation._v[3]);

    offsetEigen << translation.x, translation.y, translation.z;

    pcl::transformPointCloud(*cloud, *cloud, offsetEigen, rotationEigen);
}


//
// threshold
//
template <typename T>
inline void threshold(T cloud, const char * dimension = "z", float min = 0, float max = 1800){
    assert(cloud);

    if(cloud->points.empty()){
        return;
    }

    pcl::PassThrough <typename T::element_type::PointType> pass;
    pass.setInputCloud(cloud);
    pass.setFilterFieldName(dimension);
    pass.setFilterLimits(min, max);
    pass.filter(*cloud);
}

//
// downsample
//
template <typename T>
inline void downsample(T cloud, ofVec3f resolution = ofVec3f(0.01f, 0.01f, 0.01f)){
    assert(cloud);

    if(cloud->points.empty()){
        return;
    }

    pcl::VoxelGrid <typename T::element_type::PointType> sor;
    sor.setInputCloud(cloud);
    sor.setLeafSize(resolution.x, resolution.y, resolution.z);

    sor.filter(*cloud);
}

//
// outlier removal
//
template <typename T>
inline void statisticalOutlierRemoval(T cloud, int nr_k = 50, double std_mul = 1.0){
    assert(cloud);

    if(cloud->points.empty()){
        return;
    }

    pcl::StatisticalOutlierRemoval <typename T::element_type::PointType> sor;
    sor.setInputCloud(cloud);
    sor.setMeanK(nr_k);
    sor.setStddevMulThresh(std_mul);
    sor.filter(*cloud);
}

template <typename T>
inline void radiusOutlierRemoval(T cloud, double radius, int num_min_points){
    assert(cloud);

    if(cloud->points.empty()){
        return;
    }

    pcl::RadiusOutlierRemoval <typename T::element_type::PointType> outrem;
    outrem.setInputCloud(cloud);
    outrem.setRadiusSearch(radius);
    outrem.setMinNeighborsInRadius(num_min_points);
    outrem.filter(*cloud);
}

//
// segmentation
//
template <typename T>
inline vector <T> segmentation(T cloud, const pcl::SacModel model_type = pcl::SACMODEL_PLANE, const float distance_threshold = 1, const int min_points_limit = 10, const int max_segment_count = 30){
	assert(cloud);

    if(cloud->points.empty()){
        return;
    }

	pcl::ModelCoefficients::Ptr coefficients(new pcl::ModelCoefficients());
	pcl::PointIndices::Ptr inliers(new pcl::PointIndices());

    pcl::SACSegmentation <typename T::element_type::PointType> seg;
	seg.setOptimizeCoefficients(false);

	seg.setModelType(model_type);
	seg.setMethodType(pcl::SAC_RANSAC);
	seg.setDistanceThreshold(distance_threshold);
	seg.setMaxIterations(500);

	T temp(new typename T::element_type(*cloud));
	const size_t original_szie = temp->points.size();

    pcl::ExtractIndices <typename T::element_type::PointType> extract;
    vector <T> result;

	int segment_count = 0;
    while(temp->size() > original_szie * 0.3){
        if(segment_count > max_segment_count){
            break;
        }
		segment_count++;

		seg.setInputCloud(temp);
		seg.segment(*inliers, *coefficients);

        if(inliers->indices.size() < min_points_limit){
			break;
        }

		T filterd_point_cloud(new typename T::element_type);

		extract.setInputCloud(temp);
		extract.setIndices(inliers);
		extract.setNegative(false);
		extract.filter(*filterd_point_cloud);

        if(filterd_point_cloud->points.size() > 0){
			result.push_back(filterd_point_cloud);
		}

		extract.setNegative(true);
		extract.filter(*temp);
	}

	return result;
}

//
// normal estimation
//
template <typename T1, typename T2>
inline void normalEstimation(const T1 & cloud, T2 & output_cloud_with_normals, int kSearch = 20, double includeRadius = 5){
    if(output_cloud_with_normals == NULL){
        output_cloud_with_normals = New <T2>();
    }

	assert(cloud);

    if(cloud->points.empty()){
        return;
    }

    pcl::NormalEstimation <typename T1::element_type::PointType, NormalType> n;
	NormalPointCloud normals(new typename NormalPointCloud::element_type);

    KdTree <typename T1::element_type::PointType> kdtree(cloud);

	n.setInputCloud(cloud);
	n.setSearchMethod(kdtree.kdtree);
	n.setKSearch(kSearch);
    n.setRadiusSearch(includeRadius);
	n.compute(*normals);

	pcl::concatenateFields(*cloud, *normals, *output_cloud_with_normals);
}

//
// MLS
//
template <typename T1, typename T2>
void movingLeastSquares(const T1 & cloud, T2 & output_cloud_with_normals, float search_radius = 30){
    if(output_cloud_with_normals == NULL){
        output_cloud_with_normals = New <T2>();
    }

	assert(cloud);

    if(cloud->points.empty()){
        return;
    }

    KdTree <typename T1::element_type::PointType> kdtree;

    pcl::MovingLeastSquares <
		typename T1::element_type::PointType,
		typename T2::element_type::PointType
        > mls;

	mls.setComputeNormals(true);

	// Set parameters
	mls.setInputCloud(cloud);
	mls.setPolynomialOrder(true);
	mls.setSearchMethod(kdtree.kdtree);
	mls.setSearchRadius(search_radius);

	// Reconstruct
	mls.process(*output_cloud_with_normals);
}

//
// triangulate
//
template <typename T>
ofMesh triangulate(const T & cloud_with_normals, float search_radius = 30, double nearestNeighbourMult =  2.5, double maxNearNeighb = 20, double maxSurfaceAngle  = 180, double minAngle = 5, double maxAngle = 120){
	assert(cloud_with_normals);

	ofMesh mesh;

    if(cloud_with_normals->points.empty()){
        return mesh;
    }

    KdTree <typename T::element_type::PointType> kdtree(cloud_with_normals);

    typename pcl::GreedyProjectionTriangulation <typename T::element_type::PointType> gp3;
	pcl::PolygonMesh triangles;

	// Set the maximum distance between connected points (maximum edge length)
	gp3.setSearchRadius(search_radius);

	gp3.setMu(nearestNeighbourMult);

	gp3.setMaximumNearestNeighbors(maxNearNeighb);
	gp3.setMaximumSurfaceAngle(ofDegToRad(maxSurfaceAngle));
	gp3.setMinimumAngle(ofDegToRad(minAngle));
	gp3.setMaximumAngle(ofDegToRad(maxAngle));
	gp3.setNormalConsistency(true);

	gp3.setInputCloud(cloud_with_normals);
	gp3.setSearchMethod(kdtree.kdtree);
	gp3.reconstruct(triangles);

	convert(cloud_with_normals, mesh);

    for(int i = 0; i < triangles.polygons.size(); i++){
        pcl::Vertices & v = triangles.polygons[i];

        if(v.vertices.size() == 3){
			mesh.addTriangle(v.vertices[0], v.vertices[1], v.vertices[2]);
        }
    }

	return mesh;
}

template <typename T>
ofMesh triangulatePoisson(const T & cloud_with_normals, float depth = 9){
    assert(cloud_with_normals);

    ofMesh mesh;

    if(cloud_with_normals->points.empty()){
        return mesh;
    }

    KdTree <typename T::element_type::PointType> kdtree(cloud_with_normals);

    pcl::Poisson <typename T::element_type::PointType> poisson;
    poisson.setDepth(depth);
    poisson.setInputCloud(cloud_with_normals);
    pcl::PolygonMesh triangles;
    poisson.reconstruct(triangles);


    convert(cloud_with_normals, mesh);

    for(int i = 0; i < triangles.polygons.size(); i++){
        pcl::Vertices & v = triangles.polygons[i];

        if(v.vertices.size() == 3){
            mesh.addTriangle(v.vertices[0], v.vertices[1], v.vertices[2]);
        }
    }

    return mesh;
}

//
// GridProjection # dosen't work...?
//
template <typename T>
ofMesh gridProjection(const T & cloud_with_normals, float resolution = 1, int padding_size = 3){
	ofMesh mesh;

    if(cloud_with_normals->points.empty()){
        return mesh;
    }

    typename pcl::KdTreeFLANN <typename T::element_type::PointType>::Ptr tree(new pcl::KdTreeFLANN <typename T::element_type::PointType> );
	tree->setInputCloud(cloud_with_normals);

    pcl::GridProjection <typename T::element_type::PointType> gp;
	pcl::PolygonMesh triangles;

	gp.setResolution(resolution);
	gp.setPaddingSize(padding_size);
	gp.setNearestNeighborNum(30);

	// Get result
	gp.setInputCloud(cloud_with_normals);
	gp.setSearchMethod(tree);
	gp.reconstruct(triangles);

	convert(cloud_with_normals, mesh);

    for(int i = 0; i < triangles.polygons.size(); i++){
        pcl::Vertices & v = triangles.polygons[i];

        if(v.vertices.size() == 4){
			mesh.addTriangle(v.vertices[0], v.vertices[1], v.vertices[2]);
			mesh.addTriangle(v.vertices[2], v.vertices[3], v.vertices[0]);
		}
	}

	return mesh;
}

template <typename T>
void pairAlignMatrix(const T & cloud_src,
                     const T & cloud_tgt,
                     glm::mat4 & outputMatrix,
                     ColorPointCloud & output_src,
                     ColorPointCloud & output_tgt,
                     const string src_filename,
                     const string tgt_filename,
                     float fitnessResult = 0,
                     const bool downsample = false,
                     const ofVec3f resolution = ofVec3f(6.0f, 6.0f, 6.0f),
                     const float transformationEpsilon = 0.1,
                     const float transforRotationEpsilon = 0.8,
                     const int iterations = 80,
                     const int maxIterations = 10,
                     const float corrsepondance = 900,
                     const float normalSearchRadius = 30,
                     const float incrementalCorrespondanceReduction = 20){
    ofMesh mesh;
    // Downsample for consistency and speed
    // \note enable this for large datasets

    pcl::VoxelGrid <typename T::element_type::PointType> grid;
    T src(new typename T::element_type(*cloud_src));
    T tgt(new typename T::element_type(*cloud_tgt));

    if(downsample){
        grid.setLeafSize(resolution.x, resolution.y, resolution.z);
        grid.setInputCloud(cloud_src);
        grid.filter(*src);

        grid.setInputCloud(cloud_tgt);
        grid.filter(*tgt);
    }else{
        src = cloud_src;
        tgt = cloud_tgt;
    }


    // Compute surface normals and curvature
    PointCloudWithNormals::Ptr points_with_normals_src(new PointCloudWithNormals);
    PointCloudWithNormals::Ptr points_with_normals_tgt(new PointCloudWithNormals);

    pcl::NormalEstimation <PointT, PointNormalT> norm_est;
    pcl::search::KdTree <pcl::PointXYZ>::Ptr tree(new pcl::search::KdTree <pcl::PointXYZ> ());
    norm_est.setSearchMethod(tree);
    norm_est.setKSearch(normalSearchRadius);

    norm_est.setInputCloud(src);
    norm_est.compute(*points_with_normals_src);
    pcl::copyPointCloud(*src, *points_with_normals_src);

    norm_est.setInputCloud(tgt);
    norm_est.compute(*points_with_normals_tgt);
    pcl::copyPointCloud(*tgt, *points_with_normals_tgt);

    // Align
    pcl::IterativeClosestPointNonLinear <PointNormalT, PointNormalT> reg;
    //reg.setTransformationEpsilon (1e-6);
    reg.setTransformationEpsilon(transformationEpsilon);
    #if PCL_VERSION_COMPARE(>=, 1, 9, 1)
        cout << "PCL_VERSION " <<  PCL_VERSION_PRETTY << endl;
        reg.setTransformationRotationEpsilon(transforRotationEpsilon);
    #endif

    // Set the maximum distance between two correspondences (src<->tgt) to 10cm
    // Note: adjust this based on the size of your datasets
    reg.setMaxCorrespondenceDistance(corrsepondance);
    // Set the point representation

    reg.setInputSource(points_with_normals_src);
    reg.setInputTarget(points_with_normals_tgt);

    //
    // Run the same optimization in a loop
    Eigen::Matrix4f Ti = Eigen::Matrix4f::Identity(), prev, targetToSource;
    PointCloudWithNormals::Ptr reg_result = points_with_normals_src;
    reg.setMaximumIterations(maxIterations);
    for(int i = 0; i < iterations; ++i){
        PCL_INFO("Iteration Nr. %d.\n", i);

        // save cloud for visualization purpose
        points_with_normals_src = reg_result;

        // Estimate
        reg.setInputSource(points_with_normals_src);
        reg.align(*reg_result);

        //accumulate transformation between each Iteration
        Ti = reg.getFinalTransformation() * Ti;

        //if the difference between this transformation and the previous one
        //is smaller than the threshold, refine the process by reducing
        //the maximal correspondence distance
        if(std::abs((reg.getLastIncrementalTransformation() - prev).sum()) < reg.getTransformationEpsilon()){
            reg.setMaxCorrespondenceDistance(reg.getMaxCorrespondenceDistance() - incrementalCorrespondanceReduction);
            cout << "getMaxCorrespondenceDistance: " + ofToString(reg.getMaxCorrespondenceDistance()) + " getTransformationEpsilon " + ofToString(reg.getTransformationEpsilon()) << endl;
        }
        prev = reg.getLastIncrementalTransformation();
        cout << ofToString(reg.getFitnessScore()) << endl;
        fitnessResult = reg.getFitnessScore();
    }

    //
    // Get the transformation from target to source
    targetToSource = Ti.inverse();
    outputMatrix = glm::make_mat4x4(targetToSource.data());
//    outputMatrix = glm::mat4(targetToSource.data());

    pcl::transformPointCloud(*output_tgt, *output_tgt, targetToSource);

    ofxPCL::savePointCloud(src_filename, output_src);
    ofxPCL::savePointCloud(tgt_filename, output_tgt);

}
template <typename T, typename T1>
ofMesh pairAlign(const T & cloud_src, const T & cloud_tgt,   T1 & output, bool downsample = false, ofVec3f resolution = ofVec3f(6.0f, 6.0f, 6.0f), float transformationEpsilon = 0.1, float transforRotationEpsilon = 0.8, int iterations = 80, int maxIterations = 10, float corrsepondance = 900, float normalSearchRadius = 30, float incrementalCorrespondanceReduction = 20, float fitnessResult = 0, std::string targetPath = ""){
    ofMesh mesh;
    if(output == NULL){
        output = New <T1>();
    }
    // Downsample for consistency and speed
    // \note enable this for large datasets

    pcl::VoxelGrid <typename T::element_type::PointType> grid;
    T src(new typename T::element_type(*cloud_src));
    T tgt(new typename T::element_type(*cloud_tgt));

    if(downsample){
        grid.setLeafSize(resolution.x, resolution.y, resolution.z);
        grid.setInputCloud(cloud_src);
        grid.filter(*src);

        grid.setInputCloud(cloud_tgt);
        grid.filter(*tgt);
    }else{
        src = cloud_src;
        tgt = cloud_tgt;
    }


    // Compute surface normals and curvature
    PointCloudWithNormals::Ptr points_with_normals_src(new PointCloudWithNormals);
    PointCloudWithNormals::Ptr points_with_normals_tgt(new PointCloudWithNormals);

    pcl::NormalEstimation <PointT, PointNormalT> norm_est;
    pcl::search::KdTree <pcl::PointXYZ>::Ptr tree(new pcl::search::KdTree <pcl::PointXYZ> ());
    norm_est.setSearchMethod(tree);
    norm_est.setKSearch(normalSearchRadius);

    norm_est.setInputCloud(src);
    norm_est.compute(*points_with_normals_src);
    pcl::copyPointCloud(*src, *points_with_normals_src);

    norm_est.setInputCloud(tgt);
    norm_est.compute(*points_with_normals_tgt);
    pcl::copyPointCloud(*tgt, *points_with_normals_tgt);

    // Align
    pcl::IterativeClosestPointNonLinear <PointNormalT, PointNormalT> reg;
    //reg.setTransformationEpsilon (1e-6);
    reg.setTransformationEpsilon(transformationEpsilon);
    #if PCL_VERSION_COMPARE(>=, 1, 9, 1)
        cout << "PCL_VERSION " <<  PCL_VERSION_PRETTY << endl;
        reg.setTransformationRotationEpsilon(transforRotationEpsilon);
    #endif

    // Set the maximum distance between two correspondences (src<->tgt) to 10cm
    // Note: adjust this based on the size of your datasets
    reg.setMaxCorrespondenceDistance(corrsepondance);
    // Set the point representation

    reg.setInputSource(points_with_normals_src);
    reg.setInputTarget(points_with_normals_tgt);




    //
    // Run the same optimization in a loop and visualize the results
    Eigen::Matrix4f Ti = Eigen::Matrix4f::Identity(), prev, targetToSource;
    PointCloudWithNormals::Ptr reg_result = points_with_normals_src;
    reg.setMaximumIterations(maxIterations);
    for(int i = 0; i < iterations; ++i){
        PCL_INFO("Iteration Nr. %d.\n", i);

        // save cloud for visualization purpose
        points_with_normals_src = reg_result;

        // Estimate
        reg.setInputSource(points_with_normals_src);
        reg.align(*reg_result);

        //accumulate transformation between each Iteration
        Ti = reg.getFinalTransformation() * Ti;

        //if the difference between this transformation and the previous one
        //is smaller than the threshold, refine the process by reducing
        //the maximal correspondence distance
        if(std::abs((reg.getLastIncrementalTransformation() - prev).sum()) < reg.getTransformationEpsilon()){
            reg.setMaxCorrespondenceDistance(reg.getMaxCorrespondenceDistance() - incrementalCorrespondanceReduction);
            cout << "getMaxCorrespondenceDistance: " + ofToString(reg.getMaxCorrespondenceDistance()) + " getTransformationEpsilon " + ofToString(reg.getTransformationEpsilon()) << endl;
        }
        prev = reg.getLastIncrementalTransformation();
        cout << ofToString(reg.getFitnessScore()) << endl;
        fitnessResult = reg.getFitnessScore();
        // visualize current state
    }

    //
    // Get the transformation from target to source
    targetToSource = Ti.inverse();

    //
    // Transform target back in source frame
    pcl::transformPointCloud(*cloud_tgt, *output, targetToSource);

    ofFile input(ofToDataPath(targetPath, true));

    input.getEnclosingDirectory();

    ofxPCL::ColorPointCloud saveCloud(new ofxPCL::ColorPointCloud::element_type);
    pcl::io::loadPCDFile(ofToDataPath(targetPath, true), *saveCloud);
    pcl::transformPointCloud(*saveCloud, *saveCloud, targetToSource);
    ofxPCL::savePointCloud(ofToDataPath(targetPath, true).substr(0, ofToDataPath(targetPath, true).length() - 3) + "_aligned.pcd", saveCloud);

    ofFile transformationFile;
    transformationFile.open(input.getEnclosingDirectory() + ofGetTimestampString() + "targetToSource.json", ofFile::WriteOnly, false);
    transformationFile << "["
                       << targetToSource(0, 0) << "," << targetToSource(1, 0) << "," << targetToSource(2, 0) << "," << targetToSource(3, 0) << ","
                       << targetToSource(0, 1) << "," << targetToSource(1, 1) << "," << targetToSource(2, 1) << "," << targetToSource(3, 1) << ","
                       << targetToSource(0, 2) << "," << targetToSource(1, 2) << "," << targetToSource(2, 2) << "," << targetToSource(3, 2) << ","
                       << targetToSource(0, 3) << "," << targetToSource(1, 3) << "," << targetToSource(2, 3) << "," << targetToSource(3, 3) << "]";
    transformationFile.close();

    //color_to_depth.ply

    //add the source to the transformed target
    *output += *cloud_src;

    //final_transform = targetToSource;

    convert(output, mesh);
    return mesh;
}

template <typename T, typename T1>
ofMesh pairAlignCumulative(const T & cloud_src, const T & cloud_tgt,   T1 & output, bool downsample = false, ofVec3f resolution = ofVec3f(6.0f, 6.0f, 6.0f), float transformationEpsilon = 0.1, float transforRotationEpsilon = 0.8, int iterations = 80, int maxIterations = 10, float corrsepondance = 900, float normalSearchRadius = 30, float incrementalCorrespondanceReduction = 20, float fitnessResult = 0, std::string srcPath = "", std::string targetPath = ""){
    ofMesh mesh;
    if(output == NULL){
        output = New <T1>();
    }
    // Downsample for consistency and speed
    // \note enable this for large datasets

    pcl::VoxelGrid <typename T::element_type::PointType> grid;
    T src(new typename T::element_type(*cloud_src));
    T tgt(new typename T::element_type(*cloud_tgt));

    if(downsample){
        grid.setLeafSize(resolution.x, resolution.y, resolution.z);
        grid.setInputCloud(cloud_src);
        grid.filter(*src);

        grid.setInputCloud(cloud_tgt);
        grid.filter(*tgt);
    }else{
        src = cloud_src;
        tgt = cloud_tgt;
    }


    // Compute surface normals and curvature
    PointCloudWithNormals::Ptr points_with_normals_src(new PointCloudWithNormals);
    PointCloudWithNormals::Ptr points_with_normals_tgt(new PointCloudWithNormals);

    pcl::NormalEstimation <PointT, PointNormalT> norm_est;
    pcl::search::KdTree <pcl::PointXYZ>::Ptr tree(new pcl::search::KdTree <pcl::PointXYZ> ());
    norm_est.setSearchMethod(tree);
    norm_est.setKSearch(normalSearchRadius);

    norm_est.setInputCloud(src);
    norm_est.compute(*points_with_normals_src);
    pcl::copyPointCloud(*src, *points_with_normals_src);

    norm_est.setInputCloud(tgt);
    norm_est.compute(*points_with_normals_tgt);
    pcl::copyPointCloud(*tgt, *points_with_normals_tgt);

    // Align
    pcl::IterativeClosestPointNonLinear <PointNormalT, PointNormalT> reg;
    //reg.setTransformationEpsilon (1e-6);
    reg.setTransformationEpsilon(transformationEpsilon);
    #if PCL_VERSION_COMPARE(>=, 1, 9, 1)
        reg.setTransformationRotationEpsilon(transforRotationEpsilon);
    #endif
    // Set the maximum distance between two correspondences (src<->tgt) to 10cm
    // Note: adjust this based on the size of your datasets
    reg.setMaxCorrespondenceDistance(corrsepondance);
    // Set the point representation

    reg.setInputSource(points_with_normals_src);
    reg.setInputTarget(points_with_normals_tgt);




    //
    // Run the same optimization in a loop and visualize the results
    Eigen::Matrix4f Ti = Eigen::Matrix4f::Identity(), prev, targetToSource;
    PointCloudWithNormals::Ptr reg_result = points_with_normals_src;
    reg.setMaximumIterations(maxIterations);
    for(int i = 0; i < iterations; ++i){
        PCL_INFO("Iteration Nr. %d.\n", i);

        // save cloud for visualization purpose
        points_with_normals_src = reg_result;

        // Estimate
        reg.setInputSource(points_with_normals_src);
        reg.align(*reg_result);

        //accumulate transformation between each Iteration
        Ti = reg.getFinalTransformation() * Ti;

        //if the difference between this transformation and the previous one
        //is smaller than the threshold, refine the process by reducing
        //the maximal correspondence distance
        if(std::abs((reg.getLastIncrementalTransformation() - prev).sum()) < reg.getTransformationEpsilon()){
            reg.setMaxCorrespondenceDistance(reg.getMaxCorrespondenceDistance() - incrementalCorrespondanceReduction);
            cout << "getMaxCorrespondenceDistance: " + ofToString(reg.getMaxCorrespondenceDistance()) + " getTransformationEpsilon " + ofToString(reg.getTransformationEpsilon()) << endl;
        }
        prev = reg.getLastIncrementalTransformation();
        cout << ofToString(reg.getFitnessScore()) << endl;
        fitnessResult = reg.getFitnessScore();
        // visualize current state
    }

    //
    // Get the transformation from target to source
    targetToSource = Ti.inverse();

    //
    // Transform target back in source frame
    pcl::transformPointCloud(*cloud_tgt, *output, targetToSource);

    ofFile input(ofToDataPath(targetPath, true));

    input.getEnclosingDirectory();

    ofxPCL::ColorPointCloud transformedCloud(new ofxPCL::ColorPointCloud::element_type);
    ofxPCL::ColorPointCloud orignalCloud(new ofxPCL::ColorPointCloud::element_type);
    pcl::io::loadPCDFile(srcPath, *orignalCloud);

    pcl::io::loadPCDFile(targetPath, *transformedCloud);
    pcl::transformPointCloud(*transformedCloud, *transformedCloud, targetToSource);
    *orignalCloud += *transformedCloud;
    ofxPCL::savePointCloud(input.getEnclosingDirectory() + ofGetTimestampString() + "combined.pcd", orignalCloud);

    //color_to_depth.ply

    ofFile transformationFile;
    transformationFile.open(input.getEnclosingDirectory() + ofGetTimestampString() + "targetToSourceCombined.json", ofFile::WriteOnly, false);
    transformationFile << "["
                       << targetToSource(0, 0) << "," << targetToSource(1, 0) << "," << targetToSource(2, 0) << "," << targetToSource(3, 0)
                       << targetToSource(0, 1) << "," << targetToSource(1, 1) << "," << targetToSource(2, 1) << "," << targetToSource(3, 1)
                       << targetToSource(0, 2) << "," << targetToSource(1, 2) << "," << targetToSource(2, 2) << "," << targetToSource(3, 2)
                       << targetToSource(0, 3) << "," << targetToSource(1, 3) << "," << targetToSource(2, 3) << "," << targetToSource(3, 3) << "]";
    transformationFile.close();


    //add the source to the transformed target
    *output += *cloud_src;

    //final_transform = targetToSource;

    convert(orignalCloud, mesh);
    return mesh;
}
template <typename T>
void cropBox(T cloud, ofMatrix4x4 matrix, float minX = 0, float minY = 0, float minZ = 0, float maxX = 0, float maxY = 0, float maxZ = 0){
    assert(cloud);

    if(cloud->points.empty()){
        return;
    }

//    pcl::CropBox<typename T::element_type::PointType> boxFilter;
//    boxFilter.setMin(Eigen::Vector4f(minX, minY, minZ, 1.0));
//    boxFilter.setMax(Eigen::Vector4f(maxX, maxY, maxZ, 1.0));
//    boxFilter.setInputCloud(cloud);
//    boxFilter.filter(*cloud);
//
//    pcl::transformPointCloud(*cloud, *cloud, eigenMat);
}


ofMesh organizedFastMesh(const ofPixels & colorImage, const ofShortPixels & depthImage, const int skip = 4);

template <typename T>
void integralImageNormalEstimation(const T & cloud, NormalPointCloud & normals){
	assert(cloud->isOrganized());

    if(!normals){
        normals = New <NormalPointCloud>();
    }

    pcl::IntegralImageNormalEstimation <typename T::element_type::PointType, NormalType> ne;

    ne.setNormalEstimationMethod(pcl::IntegralImageNormalEstimation <typename T::element_type::PointType, NormalType>::AVERAGE_3D_GRADIENT);

	ne.setMaxDepthChangeFactor(10.0f);
	ne.setNormalSmoothingSize(2.0f);
	ne.setInputCloud(cloud);
	ne.compute(*normals);
}


}

template <typename T>

ofMesh organizedFastMeshCloud(const T & cloud, const int skip = 2){


    pcl::OrganizedFastMesh <typename T::element_type::PointType> ofm;
    ofm.setTrianglePixelSize(1);
    ofm.setTriangulationType(pcl::OrganizedFastMesh <typename T::element_type::PointType>::TRIANGLE_RIGHT_CUT);
    ofm.setInputCloud(cloud);

    boost::shared_ptr <std::vector <pcl::Vertices> > verts(new std::vector <pcl::Vertices> );
    ofm.reconstruct(*verts);

    ofxPCL::NormalPointCloud normals(new typename ofxPCL::NormalPointCloud::element_type);
    ofxPCL::integralImageNormalEstimation(cloud, normals);

    ofMesh mesh;

    for(int i = 0; i < verts->size(); i++){
        const pcl::Vertices & v = verts->at(i);

        if(v.vertices.size() != 3){
            continue;
        }

        const pcl::PointXYZRGB & p1 = cloud->points[v.vertices[0]];
        const pcl::PointXYZRGB & p2 = cloud->points[v.vertices[1]];
        const pcl::PointXYZRGB & p3 = cloud->points[v.vertices[2]];

        const pcl::Normal & n1 = normals->points[v.vertices[0]];
        const pcl::Normal & n2 = normals->points[v.vertices[1]];
        const pcl::Normal & n3 = normals->points[v.vertices[2]];

        mesh.addColor(ofColor(p1.r, p1.g, p1.b));
        mesh.addColor(ofColor(p2.r, p2.g, p2.b));
        mesh.addColor(ofColor(p3.r, p3.g, p3.b));

        mesh.addNormal(ofVec3f(-n1.normal_x, -n1.normal_y, -n1.normal_z));
        mesh.addNormal(ofVec3f(-n2.normal_x, -n2.normal_y, -n2.normal_z));
        mesh.addNormal(ofVec3f(-n3.normal_x, -n3.normal_y, -n3.normal_z));

        mesh.addVertex(ofVec3f(p1.x, p1.y, p1.z));
        mesh.addVertex(ofVec3f(p2.x, p2.y, p2.z));
        mesh.addVertex(ofVec3f(p3.x, p3.y, p3.z));
    }

    mesh.setMode(OF_PRIMITIVE_TRIANGLES);

    return mesh;

}
