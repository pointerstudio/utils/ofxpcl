#!/bin/bash

########################
# install pcl on linux #
########################

            
############
# user defined variables
#
# BUILD_TYPE can be set to Debug or Release

PCL_VERSION=1.9.1
BUILD_DIR=/tmp/pclinstall
BUILD_TYPE=Release
INSTALL_DIR=/usr/local

############
# utility functions

PROGRESS_COMMENT () {
    echo ""
    echo "########################"
    echo "# install pcl on linux #"
    echo "########################"
    echo "-> ${1}"
    echo ""
}

PROGRESS_COMMENT "verification of user defined variables"
echo "PCL_VERSION: ${PCL_VERSION}"
echo "BUILD_DIR: ${BUILD_DIR}"
echo "BUILD_TYPE: ${BUILD_TYPE}"
echo "INSTALL_DIR: ${INSTALL_DIR}"
echo ""
echo "does this sound alright?"
read -p "(y/n) " -n 1 -r
echo ""
if [[ $REPLY =~ ^[Yy]$ ]]
then
    echo "alright"
else
    echo "you may want to adjust variables in ${0}"
    exit 1
fi

PROGRESS_COMMENT "verification of user system"
echo "are you on ubuntu or debian?"
read -p "(y/n) " -n 1 -r
echo ""
if [[ $REPLY =~ ^[Yy]$ ]]
then
    echo "alright"
else
    echo "you may want to adjust distribution specific steps in ${0}"
    exit 1
fi

PROGRESS_COMMENT "install dependencies"
sudo apt install \
   libqt5webkit5 \
   libqt5webkit5-dev \
   libvtk6-dev \
   libboost-thread-dev \
   libboost-date-time-dev \
   libboost-iostreams-dev \
   libeigen3-dev \
   libflann-dev \
   libqhull-dev \
   libopenni-dev \
   -y

echo "VTK has a problem with itself."
echo "should we do 'sudo ln -s /usr/bin/vtk6 /usr/bin/vtk' to fix it?"
read -p "(y/n) " -n 1 -r
echo ""
if [[ $REPLY =~ ^[Yy]$ ]]
then
    echo "alright"
    sudo ln -s /usr/bin/vtk6 /usr/bin/vtk
else
    echo "vtk will probably not like it, but it's still gonna compile without vtk support"
fi


PROGRESS_COMMENT "setup directory ${BUILD_DIR}"
mkdir -p ${BUILD_DIR}/build
mkdir -p ${INSTALL_DIR}
cd ${BUILD_DIR}

PROGRESS_COMMENT "download pcl ${PCL_VERSION}"
wget -c https://github.com/PointCloudLibrary/pcl/archive/pcl-${PCL_VERSION}.tar.gz

PROGRESS_COMMENT "build pcl"
tar -xvzf pcl-${PCL_VERSION}.tar.gz
cd build
cmake -DCMAKE_BUILD_TYPE=${BUILD_TYPE} -DCMAKE_INSTALL_PREFIX=${INSTALL_DIR} ../pcl-pcl-${PCL_VERSION}
make -j$(expr $(nproc) / 2)
if [[ $INSTALL_DIR = '/usr/local' ]]
then
    sudo make -j$(expr $(nproc) / 2) install
else
    make -j$(expr $(nproc) / 2) install
fi

PROGRESS_COMMENT "we're done. BYE"
