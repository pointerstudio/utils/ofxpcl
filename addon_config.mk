# All variables and this file are optional, if they are not present the PG and the
# makefiles will try to parse the correct values from the file system.
#
# Variables that specify exclusions can use % as a wildcard to specify that anything in
# that position will match. A partial path can also be specified to, for example, exclude
# a whole folder from the parsed paths from the file system
#
# Variables can be specified using = or +=
# = will clear the contents of that variable both specified from the file or the ones parsed
# from the file system
# += will add the values to the previous ones in the file or the ones parsed from the file 
# system
# 
# The PG can be used to detect errors in this file, just create a new project with this addon 
# and the PG will write to the console the kind of error and in which line it is

meta:
	ADDON_NAME = ofxPCL
	ADDON_DESCRIPTION = Wrapper for PCL and data type conversion from and to openFrameworks
	ADDON_AUTHOR = satoruhiga (edited by Frederick Rodrigues, Jakob Schloetter)
	ADDON_TAGS = "pointclouds" "Point Cloud Library" "pcl" "point cloud processing"
	ADDON_URL = https://gitlab.com/pointerstudio/apop/ofxPCL

common:
	# dependencies with other addons, a list of them separated by spaces 
	# or use += in several lines
	
	# include search paths, this will be usually parsed from the file system
	# but if the addon or addon libraries need special search paths they can be
	# specified here separated by spaces or one per line using +=
	# ADDON_INCLUDES = 
	
	# any special flag that should be passed to the compiler when using this
	# addon
	# ADDON_CFLAGS =
	
	# any special flag that should be passed to the linker when using this
	# addon, also used for system libraries with -lname
	# ADDON_LDFLAGS =
	
	# linux only, any library that should be included in the project using
	# pkg-config
	# ADDON_PKG_CONFIG_LIBRARIES =
	
	# osx/iOS only, any framework that should be included in the project
	# ADDON_FRAMEWORKS =
	
	# source files, these will be usually parsed from the file system looking
	# in the src folders in libs and the root of the addon. if your addon needs
	# to include files in different places or a different set of files per platform
	# they can be specified here
	# ADDON_SOURCES =
	
	# some addons need resources to be copied to the bin/data folder of the project
	# specify here any files that need to be copied, you can use wildcards like * and ?
	# ADDON_DATA = 
	
	# when parsing the file system looking for libraries exclude this for all or
	# a specific platform
	# ADDON_LIBS_EXCLUDE =
	#
	
osx:
	ADDON_INCLUDES =  /usr/local/Cellar/pcl/1.9.1_4/include/pcl-1.9/
	ADDON_INCLUDES += /usr/local/Cellar/eigen/3.3.7/include/eigen3
	ADDON_INCLUDES += /usr/local/Cellar/boost/1.72.0/include
	ADDON_INCLUDES += /usr/local/Cellar/flann/1.9.1_7/include
	ADDON_INCLUDES += /usr/local/Cellar/qhull/2019.1/include/libqhull_r

linux64:
	ADDON_CFLAGS = -fpermissive
	ADDON_PKG_CONFIG_LIBRARIES = pcl_common-1.9 pcl_kdtree-1.9 pcl_octree-1.9 pcl_search-1.9 pcl_sample_consensus-1.9 pcl_filters-1.9 pcl_2d-1.9 pcl_geometry-1.9 pcl_io-1.9 pcl_features-1.9 pcl_ml-1.9 pcl_segmentation-1.9 pcl_visualization-1.9 pcl_surface-1.9 pcl_registration-1.9 pcl_keypoints-1.9 pcl_tracking-1.9 pcl_recognition-1.9 pcl_stereo-1.9 pcl_outofcore-1.9 pcl_people-1.9 
