# Setup instruction Linux

1. Install

On a Debian-based Linux distribution (like Ubuntu, or Debian), you should be able to simply run the provided install [script](/scripts/ci/linux/install.sh).
It has been tested on Debian 10/11 and Ubuntu 18.04/20.04/21.04.

```
cd /scripts/ci/linux
./install.sh
```
Then follow instructions. The script will ask for your user password when trying to install dependencies. If you don't feel comfortable with that, check the script and execute the steps manually.

2. Everything should work now, happy clouding.

## expected troubles
Should you compile your project with openCV as well, you might run into a clash between PCL and openCV.
Luckily it is easily solvable by adding two little pointers in `/usr/local/include/pcl-1.9/pcl/kdtree/kdtree_flann.h` as described here `https://github.com/PointCloudLibrary/pcl/issues/1481`.

We might just package a patched pcl version with this addon in the future, if there is request for it.

# Setup instruction OSX

1. Install PCL and dependencies, for OSX you can use homebrew, it will install all dependencies except for openNi


2. You can use the project generator to add the standard files for ofxPCL to a project, but it will not have any of the libraries we need.

3. Add the header search paths for all the required libraries, again as I used homebrew this means I added the following (warning updating any of these libraries will mean the folder names change unless you setup something in your PATH variable):

    /usr/local/Cellar/pcl/1.9.1_4/include/pcl-1.9/
    /usr/local/Cellar/eigen/3.3.7/include/eigen3
    /usr/local/Cellar/boost/1.72.0/include
    /usr/local/Cellar/flann/1.9.1_7/include
    /usr/local/Cellar/qhull/2019.1/include/libqhull_r

4. Change `HEADER_BOOST` definition in the openframeworks project to point to the full installation of boost, it will be something like: HEADER_BOOST =   "$(OF_PATH)/libs/boost/include"

    I changed it to:  /usr/local/Cellar/boost/1.72.0/include
    This points to my installation of boost, otherwise it will conflict with the boost version that comes with OF.
    

5. You now need to add the libraries to the project, on OSX you can skip all of the alias (simlinks) for me they were all in '/usr/local/Cellar/pcl/1.9.1_4/lib', also dont take add the libs called: 
        libpcl_simulation.1.9.1
        libpcl_simulation_io.1.9.1
        libpcl_visualization.1.9.1
        
6. Everything should work now, happy clouding.


